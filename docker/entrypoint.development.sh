#!/bin/bash
set -e
export FLASK_ENV=development
export FLASK_APP=wsgi.py
export APP_CONFIG_FILE=config.py
exec flask run --host=0.0.0.0
