#!/bin/bash
set -e
export FLASK_ENV=development
export FLASK_APP=wsgi.py
export APP_CONFIG_FILE=config.py
export SESSION_TYPE="filesystem"
export SECRET_KEY="VERYSECRETKEY"

exec flask run
