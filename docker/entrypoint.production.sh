#!/bin/bash
set -e

env FLASK_APP=worker.py
exec uwsgi --ini docker/uwsgi.ini
