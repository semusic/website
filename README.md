# Semusic

![Semusic logo](https://gitlab.com/semusic/website/raw/master/app/static/images/logo.png
 "Semusic logo")


As part of the course Knowledge and Data, the final project was designing an application that uses the principles and strength of the semantic web and then create an interactive web app, based on a domain, in this case music. The web app described in this report returns songs based on the preferences of the user, and allows the user to add these songs directly to Spotify by connecting their Spotify-account to the web app. 