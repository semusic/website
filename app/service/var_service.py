from flask import session


def save_vars(**variables: object) -> None:
    for var, value in variables.items():
        session[var] = value
