import urllib.parse
from flask import Blueprint, session, request, jsonify

blueprint = Blueprint('add_tracks', __name__, url_prefix="/add_tracks/")


@blueprint.route("/", methods=["POST"])
def add_tracks():
    # get the data from ajax
    ids = urllib.parse.unquote(request.data.decode("utf-8")).replace("data=",
                                                                     "").split(
        ",+|+,")

    # get all values from the data
    current_track = [ids[0]]
    list_name = ids[1].replace("+", " ")
    current_list = ids[2]
    track_name = session["sp"].track(current_track[0])["name"]

    # if a song has been added in this current session
    if "added" in session.keys():
        # if this song has not yet been added to this playlist, add it
        if not (current_track, current_list) in session["added"]:
            try:
                session["sp"].user_playlist_add_tracks(session["body"]["id"], \
                                                       current_list,
                                                       list(current_track))
            except Exception as e:
                return jsonify({
                                   "error": f"The following error occured: {str(e).split(':')[-1]}"})
            session["added"].append((current_track, current_list))
            return jsonify({"msg": f"Added {track_name} to {list_name}!"})

        # if this song has already been added to this playlist, let the user know
        return jsonify(
            {"error": f"{track_name} has already been added to {list_name}!"})

    # if no songs have been added in the session yet, add the song
    try:
        session["sp"].user_playlist_add_tracks(session["body"]["id"], \
                                               current_list,
                                               list(current_track))
        session["added"] = [(current_track, current_list)]
    except Exception as e:
        return jsonify(
            {"error": f"The following error occured: {str(e).split(':')[-1]}"})
    return jsonify({"msg": f"Added {track_name} to {list_name}!"})
