from urllib import parse

from flask import request, render_template, Blueprint, session, redirect

from config import Config

blueprint = Blueprint('home', __name__, url_prefix="/")

config = Config()


@blueprint.route("/", methods=["GET", "POST"])
def index():
    if request.method == "GET":
        if session.get('name') is not None:
            return render_template("index.html", name=session['name'])

        return render_template("index.html")

    # define the needed parameters
    data = {'client_id': config.SPOTIFY_CLIENT_ID,
            'response_type': 'code',
            'redirect_uri': request.base_url + "callback/",
            'scope': config.SPOTIFY_SCOPE
            }

    # encode the parameters to a URL-format
    url = parse.urlencode(data)

    # redirect the user to the spotify authorization page
    return redirect(f"https://accounts.spotify.com/authorize?{url}")
