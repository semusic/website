import random

import SPARQLWrapper as SPARQLWrapper
from flask import render_template, Blueprint, session, redirect, url_for, \
    request

from app.utils.dbpedia import DBpedia

from SPARQLWrapper import SPARQLWrapper, JSON

blueprint = Blueprint('results', __name__, url_prefix="/results/")


@blueprint.route("/", methods=["GET"])
def results():
    # load the query results
    session["query_results"] = DBpedia.query(session["query_local"])

    # Only send name and playlists to html if the user is logged in
    if "name" in session and "playlists" in session:
        return render_template("results.html",
                               name=session["name"],
                               results=session["query_results"],
                               playlists=[(l["name"], l["id"]) for l in
                                          session['playlists']['items']])
    # if the user is not logged in, only send the results to html
    return render_template("results.html", name=None,
                           results=session["query_results"], playlists=None)


@blueprint.route("/advanced/", methods=["POST"])
def advanced_results():
    # find requirements
    genre = request.form["genre"].replace("&", "%26").replace("'",
                                                              "%27").replace(
        " ", "%20")
    # if no genre is provided, query for all genres
    if not genre:
        genre = "Track"
    else:
        genre += "-track"

    # init variables
    danceability_min = request.form['dance-min']
    danceability_max = request.form['dance-max']
    duration_min = request.form['time-min']
    duration_max = request.form['time-max']
    energy_min = request.form['energy-min']
    energy_max = request.form['energy-max']
    instrumentalness_min = request.form['instrumental-min']
    instrumentalness_max = request.form['instrumental-max']
    tempo_min = request.form['tempo-min']
    tempo_max = request.form['tempo-max']
    valence_min = request.form['mood-min']
    valence_max = request.form['mood-max']

    # connect to local repository
    repo = SPARQLWrapper("https://graphdb.yunusdemir.nl/repositories/semusic")
    repo.setQuery(f"""
    PREFIX semu: <http://semusic.com/kad19/>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX dbp: <http://dbpedia.org/property/>
    PREFIX dbo: <http://dbpedia.org/ontology/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    select ?id ?artist where {{ 
        ?track a semu:{genre} ;
                foaf:name ?label ;
                semu:hasDanceability ?danceability ;
                semu:hasDuration ?duration ;
                semu:hasEnergy ?energy ;
                semu:hasId ?id ;
                semu:hasInstrumentalness ?instrumentalness ;
                semu:hasTempo ?tempo ;
                semu:hasValence ?valence ;
                semu:hasArtist/foaf:name ?artist .

                FILTER(?danceability >= {danceability_min} && ?danceability <= {danceability_max})
                FILTER(?duration >= {duration_min} && ?duration <= {duration_max})
                FILTER(?energy >= {energy_min} && ?energy <= {energy_max})
                FILTER(?instrumentalness >= {instrumentalness_min} && ?instrumentalness <= {instrumentalness_max})
                FILTER(?tempo >= {tempo_min} && ?tempo <= {tempo_max})
                FILTER(?valence >= {valence_min} && ?valence <= {valence_max})

    }} """)

    repo.setReturnFormat(JSON)
    results = repo.query().convert()
    session["query_local"] = results

    return redirect(url_for("results.results"))


@blueprint.route("/simple/", methods=["POST"])
def simple_results():
    # find requirements
    genre = request.form["genre"].replace("&", "%26").replace("'",
                                                              "%27").replace(
        " ", "%20")
    # if no genre is provided, query for all genres
    if not genre:
        genre = "Track"
    else:
        genre += "-track"

    valence_min = 0
    valence_max = 1

    tempo_min = 0
    tempo_max = 221

    duration_min = 0
    duration_max = 900000

    mood = request.form["moodoptions"]
    tempo = request.form["speedoptions"]
    duration = request.form['durationoptions']

    if mood == 'sad':
        valence_max = 0.35
    elif mood == 'happy':
        valence_min = 0.7

    if tempo == 'slow':
        tempo_max = 76
    elif tempo == 'fast':
        tempo_min = 120

    if duration == 'short':
        duration_max = 180000
    elif duration == 'long':
        duration_min = 240000

    # connect to local repository
    repo = SPARQLWrapper("https://graphdb.yunusdemir.nl/repositories/semusic")
    repo.setQuery(f"""
    PREFIX semu: <http://semusic.com/kad19/>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX dbp: <http://dbpedia.org/property/>
    PREFIX dbo: <http://dbpedia.org/ontology/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    select ?id ?artist where {{ 
        ?track a semu:{genre} ;
                foaf:name ?label ;
                semu:hasDanceability ?danceability ;
                semu:hasDuration ?duration ;
                semu:hasEnergy ?energy ;
                semu:hasId ?id ;
                semu:hasInstrumentalness ?instrumentalness ;
                semu:hasTempo ?tempo ;
                semu:hasValence ?valence ;
                semu:hasArtist/foaf:name ?artist .

                FILTER(?duration >= {duration_min} && ?duration <= {duration_max})
                FILTER(?tempo >= {tempo_min} && ?tempo <= {tempo_max})
                FILTER(?valence >= {valence_min} && ?valence <= {valence_max})

    }} """)
    repo.setReturnFormat(JSON)
    results = repo.query().convert()
    session["query_local"] = results

    return redirect(url_for("results.results"))
