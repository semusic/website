from flask import render_template, Blueprint, session, redirect, url_for, \
    flash, request

blueprint = Blueprint('profile', __name__, url_prefix="/profile/")


@blueprint.route("/", methods=["GET", "POST"])
def profile():
    # show the user their name and playlists (this should probably include
    # more relevant stuff, like an image and whatever later)
    if request.method == "GET":
        playlists = []

        for l in session["playlists"]["items"]:
            if l["images"]:
                playlists.append((l["name"], l["id"], l["tracks"]["total"],
                                  l["images"][0]["url"]))
            else:
                playlists.append(
                    (l["name"], l["id"], l["tracks"]["total"], None))

        return render_template("profile.html", name=session['name'],
                               playlists=playlists)

    flash("You are not logged in (anymore)!")
    session.clear()
    return redirect(url_for('home.index'))
