import base64

import requests
import spotipy
from flask import request, render_template, Blueprint, redirect, session

from config import Config

blueprint = Blueprint('callback', __name__, url_prefix="/callback/")

config = Config()


@blueprint.route("/", methods=["GET"])
def callback():
    # encode the client_id and client_secret
    auth = base64.urlsafe_b64encode(f"{config.SPOTIFY_CLIENT_ID}:"
                                    f"{config.SPOTIFY_CLIENT_SECRET}"
                                    .encode("utf-8"))

    # find the callback code provided by spotify and remove unnecessary
    # information
    rule = str(request.full_path)
    # if the user got to this page without a proper redirect, send them
    # to their profile page
    try:
        code = rule.split("?code=")[1].split("&")[0]
    except IndexError:
        return redirect("profile")

    # create a header
    auth_header = {
        'Authorization': 'Basic ' + str(auth, "utf-8")
    }

    # create data
    data = {
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': request.base_url,
        'scope': config.SPOTIFY_SCOPE
    }

    # post the request to the token-generator in spotify's API
    response = requests.post('https://accounts.spotify.com/api/token',
                             headers=auth_header, data=data,
                             verify=True).json()

    # find the user's info based on their token
    session["body"] = requests.get('https://api.spotify.com/v1/me', headers={
        'Authorization': 'Bearer ' + response['access_token']}, verify=True).json()

    session["name"] = session["body"]["display_name"]

    # load API
    session["sp"] = spotipy.Spotify(auth=response['access_token'])
    # Find the current user's playlists
    playlists = session["sp"].current_user_playlists()
    # add the playlist data to the variable

    session['playlists'] = playlists

    # return information to user
    return render_template("index.html", name=session["name"])
