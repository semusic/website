import random
from SPARQLWrapper import SPARQLWrapper, JSON


class DBpedia:
    @staticmethod
    def query(results):
        # connect to dbpedia
        repo = SPARQLWrapper("http://dbpedia.org/sparql")

        # init variables
        samplesize = 6
        bs = '\\"'
        query_lists = []

        # show up to 6 random results from the local repository
        if len(results["results"]["bindings"]) < 6:
            samplesize = len(results["results"]["bindings"])

        sample = random.sample(results["results"]["bindings"], samplesize)
        for result in sample:
            repo.setQuery(f"""
                PREFIX db: <http://dbpedia.org/>
                PREFIX semu: <http://semusic.com/kad19/>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                PREFIX foaf: <http://xmlns.com/foaf/0.1/>
                PREFIX dbp: <http://dbpedia.org/property/>
                PREFIX dbo: <http://dbpedia.org/ontology/>
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        
                select DISTINCT ?musician ?topic ?started ?ended where {{ 
        
                    ?music dbo:artist ?musician .
                    
                    ?musician foaf:name "{result["artist"]["value"].replace('"', bs)}"@en ;
                            foaf:isPrimaryTopicOf ?topic .
                    OPTIONAL{{?musician dbo:activeYearsEndYear ?ended .}}
                    OPTIONAL{{?musician dbo:activeYearsStartYear ?started .}}
                }}
                """)
            repo.setReturnFormat(JSON)
            temp = repo.query().convert()["results"]["bindings"]

            # set default values
            result["started"] = {}
            result["ended"] = {}
            result["topic"] = {}
            result["started"]["value"] = "Unavailable"
            result["ended"]["value"] = ""
            result["topic"]["value"] = "No discography was found"

            # if the artist had a dbpedia-page, update the dict it
            if temp:
                # if the user has a starting date, but no ending date, set them
                # to be an active artist
                if ("started" in temp[0].keys()) and (
                                                "ended" not in temp[0].keys()):
                    temp[0]["ended"] = {}
                    temp[0]["ended"]["value"] = "Present"
                result.update(temp[0])
            query_lists.append(result)

        return query_lists
