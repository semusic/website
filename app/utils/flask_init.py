import logging
from collections import defaultdict
from typing import Dict

from flask import Flask

_logger = logging.getLogger(__name__)


def initializer(name):
    """A wrapper to make sure dependencies are not inialized twice."""

    def decorator(f):
        def wrapper(flask_initializer, *args, **kwargs):
            if not flask_initializer.init_status_dict[name]:
                f(flask_initializer, *args, **kwargs)
                flask_initializer.init_status_dict[name] = True

        return wrapper

    return decorator


class Initializer:
    def __init__(self, app: Flask):
        self.app = app
        self.init_status_dict: Dict[str, bool] = defaultdict(lambda: False)

    @initializer('views')
    def init_views(self) -> None:
        from app.views import (home, profile, callback, results, add_tracks)

        self.app.register_blueprint(home.blueprint)
        self.app.register_blueprint(profile.blueprint)
        self.app.register_blueprint(callback.blueprint)
        self.app.register_blueprint(results.blueprint)
        self.app.register_blueprint(add_tracks.blueprint)

    @initializer('worker')
    def init_worker(self) -> None:
        from app import worker

        worker.config_from_object({
            'result_backend': 'rpc://',
            'task_serializer': 'pickle',
            'accept_content': ['pickle', 'json'],
            'result_serializer': 'pickle',
            'broker_url': self.app.config.get('SESSION_REDIS')
        })
