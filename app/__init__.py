from celery import Celery
from flask import Flask
from flask_session import Session

from app.utils.git_version import GitVersion
from app.utils.flask_init import Initializer

sess = Session()
worker: Celery = Celery('semusic')

version: GitVersion = GitVersion()


def static_url(url):
    return url + '?v=' + version.hash


def init_app() -> Flask:
    app = Flask(__name__, instance_relative_config=False)

    app.config.from_object('config.Config')

    app.jinja_env.auto_reload = app.debug
    app.jinja_env.globals.update(static_url=static_url)

    sess.init_app(app)

    with app.app_context():
        Initializer(app).init_views()

    return app
