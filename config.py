from os import environ
import redis


class Config:
    """Set Flask configuration vars from config.env file."""

    # General Config
    SECRET_KEY = environ.get('SECRET_KEY')

    FLASK_APP = environ.get('FLASK_APP')
    FLASK_ENV = environ.get('FLASK_ENV')

    # Flask-Session
    SESSION_TYPE = environ.get('SESSION_TYPE')

    if SESSION_TYPE == 'redis':
        SESSION_REDIS = redis.from_url(environ.get('SESSION_REDIS'))

    # Flask-Assets
    LESS_BIN = environ.get('LESS_BIN')
    ASSETS_DEBUG = environ.get('ASSETS_DEBUG')
    LESS_RUN_IN_DEBUG = environ.get('LESS_RUN_IN_DEBUG')

    # Static Assets
    STATIC_FOLDER = environ.get('STATIC_FOLDER')
    TEMPLATES_FOLDER = environ.get('TEMPLATES_FOLDER')
    COMPRESSOR_DEBUG = environ.get('COMPRESSOR_DEBUG')

    SPOTIFY_CLIENT_ID = '95cd9bb866544a5cad36ec352557c9e7'
    SPOTIFY_CLIENT_SECRET = '918e01a0ad104f42aa737059388beb65'
    SPOTIFY_SCOPE = 'user-read-private playlist-modify-public ' \
                    'playlist-modify-private playlist-read-private ' \
                    'playlist-read-collaborative '
    SPOTIFY_CACHE = '.spotipyoauthcache'
